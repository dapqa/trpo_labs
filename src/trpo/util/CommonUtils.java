package trpo.util;

import trpo.core.coord.Area;
import trpo.core.coord.Point;

/**
 * @author Nechaev Alexander
 * Содержит вспомогательные методы, которые могут быть использованы в разных местах проекта
 */
public class CommonUtils {

    /**
     * Проверяет, лежит ли точка в указанной прямоугольной области.
     * @param point Коориданты точки
     * @param area Координаты области
     * @return true, если точка лежит в области.
     */
    public static boolean liesWithin(Point point, Area area) {
        int x1 = Math.min(area.getX1(), area.getX2());
        int x2 = Math.max(area.getX1(), area.getX2());
        int y1 = Math.min(area.getY1(), area.getY2());
        int y2 = Math.max(area.getY1(), area.getY2());
        return point.getX() >= x1 && point.getX() <= x2 && point.getY() >=y1 && point.getY() <= y2;
    }

    /**
     * Проверяет, лежит ли прямоугольная область А внутри прямоугольной области Б.
     * @param innerArea Координаты области А
     * @param outerArea Координаты области Б
     * @return true, если область А лежит внутри области Б.
     */
    public static boolean liesWithin(Area innerArea, Area outerArea) {
        return liesWithin(innerArea.getFrom(), outerArea) && liesWithin(innerArea.getTo(), outerArea);
    }

}
