package trpo.core;

import trpo.core.coord.Area;

/**
 * @author Nechaev Alexander
 * Абстрактный фильтр, который имеет радиус и задается матричным обработчком и фабрикой матриц.
 * Изображения должны быть описаны массивом целых чисел, задающих RGB-коды цветов пикселей.
 */
public abstract class AbstractFilter {

    private int radius = 1;

    protected final MatrixProcessor matrixProcessor;
    protected final MatrixFactory matrixCreator;

    protected AbstractFilter(MatrixProcessor matrixProcessor, MatrixFactory matrixCreator) {
        this.matrixProcessor = matrixProcessor;
        this.matrixCreator = matrixCreator;
    }

    /**
     * Применяет фильтр ко всему изображению. Входное изображение расширяется согласно радиусу фильтра.
     * @param pixels Массив целых чисел, задающих RGB-коды цветов пикселей изображения.
     */
    public final void filter(int[][] pixels) {
        Area area = new Area(0, 0, pixels.length != 0 ? pixels[0].length - 1 : -1, pixels.length - 1);
        filter(pixels, area);
    }

    /**
     * Применяет фильтр к части изображения. Входное изображение расширяется согласно радиусу фильтра.
     * @param area Координаты области для обработки
     * @param pixels Массив целых чисел, задающих RGB-коды цветов пикселей изображения.
     */
    public void filter(int[][] pixels, Area area) {
        int height = pixels.length;
        if (height == 0) return;

        int width = pixels[0].length;
        if (width == 0) return;

        int[][] source = createSource(pixels);
        applyProcessor(source, pixels, area);
    }

    /**
     * Реализует самый частый вариант использования фильтра - создать матрицу и вызвать один раз матричный обработчик
     * на указанной области.
     * @param source Массив целых чисел, задающих RGB-коды цветов пикселей входного изображения.
     * @param dest Массив целых чисел, задающих RGB-коды цветов пикселей выходного изображения.
     * @param area Координаты области для обработки
     */
    protected void applyProcessor(int[][] source, int[][] dest, Area area) {
        double[][] matrix = matrixCreator.makeMatrix(radius * 2 + 1, radius * 2 + 1);
        matrixProcessor.process(matrix, area, source, dest);
    }

    /**
     * Расширяет входное изображение согласно радиусу фильтра. Края заполняются крайними пикселями изображения.
     * @param pixels Массив целых чисел, задающих RGB-коды цветов пикселей входного изображения.
     * @return Массив целых чисел, задающих RGB-коды цветов пикселей расширенного входного изображения.
     */
    protected int[][] createSource(int[][] pixels) {
        int height = pixels.length;
        int width = pixels[0].length;
        int[][] source = new int[height + radius * 2][width + radius * 2];

        // Углы расширенного изображения заполняются угловыми пикселями исходного
        for (int i = 0; i < radius; i++) {
            for (int j = 0; j < radius; j++) {
                source[i][j] = pixels[0][0];
                source[i + height + radius][j + width + radius] = pixels[height - 1][width - 1];
                source[i + height + radius][j] = pixels[height - 1][0];
                source[i][j  + width + radius] = pixels[0][width - 1];
            }
        }

        // Заполнение боков расширенного изображения согласно левой и правой грани исходного
        for (int i = radius; i < height + radius; i++) {
            for (int j = 0; j < radius; j++) {
                source[i][j] = pixels[i - radius][radius];
                source[i][j + width + radius - 1] = pixels[i - radius][width - 1];
            }
        }

        // Заполнение верха и низа расширенного изображения согласно верхней и нижней грани исходного
        for (int i = 0; i < radius; i++) {
            for (int j = radius; j < width + radius; j++) {
                source[i][j] = pixels[radius][j - radius];
                source[i + height + radius - 1][j] = pixels[height - 1][j - radius];
            }
        }

        // Заполнение центра расширенного изображения
        for (int i = 0; i < height; i++) {
            System.arraycopy(pixels[i], 0, source[i + radius], radius, width);
        }

        return source;
    }

    public int getRadius() {
        return radius;
    }

    public AbstractFilter setRadius(int radius) {
        this.radius = radius;
        return this;
    }
}
