package trpo.core.coord;

/**
 * @author Nechaev Alexander
 * Прямоугольная область, заданная двумя точками на координатной плоскости. Взаимное расположение точек может быть любым.
 */
public class Area {

    private Point from;
    private Point to;

    public Area(Point from, Point to) {
        this.from = from;
        this.to = to;
    }

    public Area(int x1, int y1, int x2, int y2) {
        this.from = new Point(x1, y1);
        this.to = new Point(x2, y2);
    }

    public Point getFrom() {
        return from;
    }

    public Point getTo() {
        return to;
    }

    public void setFrom(Point from) {
        this.from = from;
    }

    public void setTo(Point to) {
        this.to = to;
    }

    public int getX1() {
        return from.getX();
    }

    public int getY1() {
        return from.getY();
    }

    public int getX2() {
        return to.getX();
    }

    public int getY2() {
        return to.getY();
    }

    public void setX1(int x) {
        from.setX(x);
    }

    public void setY1(int y) {
        from.setY(y);
    }

    public void setX2(int x) {
        to.setX(x);
    }

    public void setY2(int y) {
        to.setY(y);
    }

    public int getHeight() {
        return Math.abs(to.getY() - from.getY());
    }

    public int getWidth() {
        return Math.abs(to.getX() - from.getX());
    }

}
