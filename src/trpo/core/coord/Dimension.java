package trpo.core.coord;

/**
 * @author Nechaev Alexander
 */
public enum Dimension {

    HORIZONTAL, VERTICAL

}
