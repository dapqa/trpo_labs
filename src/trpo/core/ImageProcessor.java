package trpo.core;

import trpo.core.coord.Area;

import java.awt.image.BufferedImage;
import java.awt.image.DataBufferByte;


/**
 * @author Nechaev Alexander
 * Главный класс библиотеки.
 * Содержит методы, необходимые для получения нового изображения из исходного после применения некоторого фильтра.
 * Фактически является оберткой для любого фильтра, которая выполняет необходимые преобразования представления изображений.
 */
public class ImageProcessor {

    /**
     * Применяет фильтр ко всему изображению.
     * @param bufferedImage Входное изображение
     * @param filter Фильтр
     * @return Выходное изображение
     */
    public static BufferedImage filter(BufferedImage bufferedImage, AbstractFilter filter) {
        int[][] pixels = toIntMatrix(bufferedImage);
        filter.filter(pixels);
        return fromIntMatrix(pixels);
    }

    /**
     * Применяет фильтр к части изображения.
     * @param bufferedImage Входное изображение
     * @param filter Фильтр
     * @param area Координаты области для обработки.
     * @return Выходное изображение
     */
    public static BufferedImage filter(BufferedImage bufferedImage, AbstractFilter filter, Area area) {
        int[][] pixels = toIntMatrix(bufferedImage);
        filter.filter(pixels, area);
        return fromIntMatrix(pixels);
    }

    /**
     * Преобразует входное изображение в матрицу целых чисел, описывающих RGB-коды цветов
     * @param bufferedImage Входное изображение (способ хранения цвета - 3BYTE_BGR)
     * @return Матрица целых чисел, описывающих RGB-коды цветов
     */
    private static int[][] toIntMatrix(BufferedImage bufferedImage) {
        if (bufferedImage.getType() != BufferedImage.TYPE_3BYTE_BGR) {
            throw new UnsupportedOperationException("" +
                    "Работа с таким типом изображений не реализована. Попробуйте прочитать изображение иначе, " +
                    "чтобы его тип был 3BYTE_BGR"
            );
        }

        final int height = bufferedImage.getHeight();
        final int width = bufferedImage.getWidth();

        final byte[] byteBuffer = ((DataBufferByte)bufferedImage.getRaster().getDataBuffer()).getData();
        int[] intBuffer = new int[width * height];
        for (int i = 0; i < width * height; i++) {
            intBuffer[i] = ((int) byteBuffer[i * 3 + 2] & 0xff) << 16 |
                    ((int) byteBuffer[i * 3 + 1] & 0xff) << 8 |
                    ((int) byteBuffer[i * 3] & 0xff);
        }

        int[][] res = new int[height][width];
        for (int i = 0; i < res.length; i++) {
            System.arraycopy(intBuffer, i * width, res[i], 0, width);
        }

        return res;
    }

    /**
     * Преобазует матрицу целых чисел, описывающих RGB-коды цветов, в изображение. Цвета кодируются способом 3BYTE_BGR.
     * @param pixels Матрица целых чисел, описывающих RGB-коды цветов
     * @return Выходное изображение (способ хранения цвета - 3BYTE_BGR)
     */
    private static BufferedImage fromIntMatrix(int[][] pixels) {
        final int height = pixels.length;
        final int width = pixels[0].length;

        BufferedImage bufferedImage = new BufferedImage(width, height, BufferedImage.TYPE_3BYTE_BGR);
        byte[] byteBuffer = ((DataBufferByte)bufferedImage.getRaster().getDataBuffer()).getData();
        for (int i = 0; i < height; i++) {
            for (int j = 0; j < width; j++) {
                int rgb = pixels[i][j];
                int pos = i * width + j;
                byteBuffer[pos * 3] = (byte) (rgb & 0xff);
                byteBuffer[pos * 3 + 1] = (byte) ((rgb >> 8) & 0xff);
                byteBuffer[pos * 3 + 2] = (byte) ((rgb >> 16) & 0xff);
            }
        }

        return bufferedImage;
    }

}
