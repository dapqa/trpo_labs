package trpo.core;

import trpo.core.coord.Area;

/**
 * @author Nechaev Alexander
 */
public interface MatrixProcessor {

    /**
     * Выполняет обработку изображения или его части с помощью заданной матрицы.
     * @param matrix Матрица, необходимая для фильтра
     * @param area Область изображения для обработки
     * @param source Пиксели исходного изображения, содержащие RGB-коды цветов
     * @param dest Пискели выходного изображения
     */
    void process(final double[][] matrix, final Area area, final int[][] source, int[][] dest);

}
