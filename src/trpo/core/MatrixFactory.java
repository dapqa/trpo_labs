package trpo.core;

/**
 * @author Nechaev Alexander
 */
public interface MatrixFactory {

    /**
     * Создает матрицу указанных размеров, заполняя её необходимыми значениями.
     * @param width Ширина (длина первого измерения)
     * @param height Высота (длина нулевого измерения)
     * @return Необходимая матрица
     */
    double[][] makeMatrix(int width, int height);

}
