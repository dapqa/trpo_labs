package trpo.impl.factory;

import trpo.core.MatrixFactory;

/**
 * @author Nechaev Alexander
 * Генерирует матрицы, заполненные согласно Гауссовому распределению с максимумом в центральной клетке.
 */
public class GaussianMatrixFactory implements MatrixFactory {

    @Override
    public double[][] makeMatrix(int width, int height) {
        // На самом деле, матрица не обязательно должна быть такой, но для размытия изображений используются именно такие
        if (width != height || width % 2 == 0) {
            throw new IllegalArgumentException("Матрицы свертки для размытия по Гауссу должна быть квадратной, с нечетными " +
                    "сторонами");
        }

        double radius = width / 2;
        double sigma = radius / 2.0;
        double[][] res = new double[height][width];

        // TODO Расчет можно оптимизировать
        double sum = 0;
        for (int i = 0; i < height; i++) {
            double iG = gaussian(radius, sigma, i);
            for (int j = 0; j < width; j++) {
                res[i][j] = iG * gaussian(radius, sigma, j);
                sum += res[i][j];
            }
        }

        for (int i = 0; i < height; i++) {
            for (int j = 0; j < width; j++) {
                res[i][j] /= sum;
            }
        }

        return res;
    }

    private double gaussian(double mu, double sigma, double x) {
        return  1.0 / (sigma * Math.sqrt(2 * Math.PI)) * Math.exp(-1.0 * Math.pow(x - mu, 2.0) / (2.0 * sigma * sigma));
    }

}
