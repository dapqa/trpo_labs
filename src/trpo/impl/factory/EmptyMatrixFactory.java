package trpo.impl.factory;

import trpo.core.MatrixFactory;

import java.util.HashMap;

/**
 * @author Nechaev Alexander
 * Для создания пустых матриц (используются, например, в медианном фильтре для задания размеров области)
 */
public class EmptyMatrixFactory implements MatrixFactory {

    /**
     * Чтобы лишний раз не создавать объекты и не выделять память, матрицы размером 1, 3, 5, ..., 31
     * создаются заранее
     */
    private static final HashMap<Integer, double[][]> VALUES_CACHE = new HashMap<Integer, double[][]>(){{
       for (int i = 1; i <= 31; i+= 2) {
           put(i, new double[i][i]);
       }
    }};

    @Override
    public double[][] makeMatrix(int width, int height) {
        return width == height && VALUES_CACHE.containsKey(width) ? VALUES_CACHE.get(width) : new double[height][width];
    }

}
