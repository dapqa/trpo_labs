package trpo.impl.factory;

import sun.reflect.generics.reflectiveObjects.NotImplementedException;
import trpo.core.MatrixFactory;
import trpo.core.coord.Dimension;
import trpo.impl.filter.outline.OutliningMethod;

import java.util.HashMap;

/**
 * @author Nechaev Alexander
 * Возвращает одну из предзаполненных матриц для выделения контуров в зависимости от выбранной маски.
 * Так как массивы в Java нельзя сделать неизменяемыми, выполняется копирование матрицы в новый массив;
 * иначе, если изменить массив где-то извне, он останется измнененным здесь же и будет содержать неверные значения.
 */
public class OutlineMatrixFactory implements MatrixFactory {

    // Для вычисления градиентов по горизонтали
    private static final HashMap<OutliningMethod, double[][]> X_MAP = new HashMap<OutliningMethod, double[][]>(){{
        put(OutliningMethod.ROBERTS, new double[][]{
                new double[]{ 0, 0,  0 },
                new double[]{ 0, 0, -1 },
                new double[]{ 0, 1,  0 }
        });
        put(OutliningMethod.PREWITT, new double[][]{
                new double[]{ -1, 0, 1 },
                new double[]{ -1, 0, 1 },
                new double[]{ -1, 0, 1 }
        });
        put(OutliningMethod.SOBEL, new double[][]{
                new double[]{ -1, 0, 1 },
                new double[]{ -2, 0, 2 },
                new double[]{ -1, 0, 1 }
        });
    }};

    // Для вычисления градиентов по вертикали
    private static final HashMap<OutliningMethod, double[][]> Y_MAP = new HashMap<OutliningMethod, double[][]>(){{
        put(OutliningMethod.ROBERTS, new double[][]{
                new double[]{ 0,  0, 0 },
                new double[]{ 0, -1, 0 },
                new double[]{ 0,  0, 1 }
        });
        put(OutliningMethod.PREWITT, new double[][]{
                new double[]{ -1, -1, -1 },
                new double[]{  0,  0,  0 },
                new double[]{  1,  1,  1 }
        });
        put(OutliningMethod.SOBEL, new double[][]{
                new double[]{ -1, -2, -1 },
                new double[]{  0,  0,  0 },
                new double[]{  1,  2,  1 }
        });
    }};

    private OutliningMethod outliningMethod;
    private Dimension dimension;

    public OutlineMatrixFactory(OutliningMethod outliningMethod, Dimension dimension) {
        this.outliningMethod = outliningMethod;
        this.dimension = dimension;
    }

    @SuppressWarnings("ManualArrayCopy")
    @Override
    public double[][] makeMatrix(int width, int height) {
        if (width != 3 || height != 3) {
            throw new NotImplementedException();
        }


        double[][] resToCopy = dimension == Dimension.HORIZONTAL ? X_MAP.get(outliningMethod) : Y_MAP.get(outliningMethod);
        double[][] res = new double[3][3];
        for (int i = 0; i < 3; i++) {
            for (int j = 0; j < 3; j++) {
                res[i][j] = resToCopy[i][j];
            }
        }

        return res;
    }

    public OutliningMethod getOutliningMethod() {
        return outliningMethod;
    }

    public void setOutliningMethod(OutliningMethod outliningMethod) {
        this.outliningMethod = outliningMethod;
    }

    public Dimension getDimension() {
        return dimension;
    }

    public void setDimension(Dimension dimension) {
        this.dimension = dimension;
    }
}
