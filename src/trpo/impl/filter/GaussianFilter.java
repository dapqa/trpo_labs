package trpo.impl.filter;

import trpo.core.AbstractFilter;
import trpo.impl.factory.GaussianMatrixFactory;
import trpo.impl.processor.ConvolutionMatrixProcessor;

/**
 * @author Nechaev Alexander
 * Фильтр для выполнения размытия по Гауссу.
 */
public class GaussianFilter extends AbstractFilter {

    public GaussianFilter() {
        super(new ConvolutionMatrixProcessor(), new GaussianMatrixFactory());
    }

}
