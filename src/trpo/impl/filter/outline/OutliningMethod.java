package trpo.impl.filter.outline;

/**
 * @author Nechaev Alexander
 * Используется для выбора маски при выделении контуров
 */
public enum OutliningMethod {

    ROBERTS, PREWITT, SOBEL

}
