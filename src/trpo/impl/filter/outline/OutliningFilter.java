package trpo.impl.filter.outline;

import trpo.core.AbstractFilter;
import trpo.core.coord.Area;
import trpo.core.coord.Dimension;
import trpo.impl.factory.OutlineMatrixFactory;
import trpo.impl.processor.BrightnessConvolutionMatrixProcessor;

/**
 * @author Nechaev Alexander
 * Фильтр для выделения контуров изображения. По умолчанию используется маска Превитта.
 * Итоговое изображение получается черно-белым, где интенсивность белого цвета зависит от величины
 * градиента изменения яркости.
 */
public class OutliningFilter extends AbstractFilter {

    public OutliningFilter() {
        super(new BrightnessConvolutionMatrixProcessor(), new OutlineMatrixFactory(OutliningMethod.PREWITT, Dimension.VERTICAL));
    }

    @Override
    protected void applyProcessor(int[][] source, int[][] dest, Area area) {
        if (area.getHeight() != dest.length - 1 || area.getWidth() != (dest.length != 0 ? dest[0].length : 0) - 1) {
            throw new UnsupportedOperationException("Выделение контуров реализовано только для изображения полностью.");
        }

        // Заводится дополнительный массив для вычисления величин градиента по горизонтали
        // Значения градиента по вертикали записываются в массив dest
        int[][] destHor = new int[dest.length][dest[0].length];

        // Вычисление величин градиента по вертикали
        getOutlineMatrixFactory().setDimension(Dimension.VERTICAL);
        super.applyProcessor(source, dest, area);

        // Вычислиение величин градиента по горизонтали
        getOutlineMatrixFactory().setDimension(Dimension.HORIZONTAL);
        super.applyProcessor(source, destHor, area);

        // В массив dest записывается суммарная длина вектора градиента, а также находятся его
        // максимальное и минимальное значение для нормализации
        double minGrad = Integer.MAX_VALUE, maxGrad = 0;
        for (int i = 0; i < dest.length; i++) {
            for (int j = 0; j < dest[0].length; j++) {
                int valVer = dest[i][j];
                int valHor = destHor[i][j];
                int valRes = ((int) Math.sqrt(valVer * valVer + valHor * valHor));
                dest[i][j] = valRes;

                minGrad = Math.min(minGrad, valRes);
                maxGrad = Math.max(maxGrad, valRes);
            }
        }

        // Нормализация
        for (int i = 0; i < dest.length; i++) {
            for (int j = 0; j < dest[0].length; j++) {
                int normalized = 255 - ((int)(255 * (dest[i][j] - minGrad / (maxGrad - minGrad))) & 0xff);
                // По некоторым причинам, градиент иногда принимает макс. значения в тех точках, где не должен;
                // поэтому пиксели, цвет которых выбран как #ffffff, должны быть на самом деле черными.
                if (normalized == 255) {
                    normalized = 0;
                }
                dest[i][j] = normalized << 16 | normalized << 8 | normalized;
            }
        }
    }

    private OutlineMatrixFactory getOutlineMatrixFactory() {
        return (OutlineMatrixFactory) matrixCreator;
    }

    @Override
    public AbstractFilter setRadius(int radius) {
        if (radius != 1) {
            throw new UnsupportedOperationException("Выделение контуров реализовано только для радиуса 1");
        }
        return this;
    }

    public OutliningFilter setMethod(OutliningMethod method) {
        getOutlineMatrixFactory().setOutliningMethod(method);
        return this;
    }

    public OutliningMethod getMethod() {
        return getOutlineMatrixFactory().getOutliningMethod();
    }
}
