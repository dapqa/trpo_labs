package trpo.impl.filter;

import trpo.core.AbstractFilter;
import trpo.impl.factory.EmptyMatrixFactory;
import trpo.impl.processor.MedianMatrixProcessor;

/**
 * @author Nechaev Alexander
 * Медианный фильтр для устранения шумов на изображении.
 */
public class MedianFilter extends AbstractFilter {

    public MedianFilter() {
        super(new MedianMatrixProcessor(), new EmptyMatrixFactory());
    }

}
