package trpo.impl.processor;

import java.util.Arrays;

/**
 * @author Nechaev Alexander
 * Использует пустую матрицу, определяющую размер области для обработки.
 * Для каждого пикселя исходного изображения формируются массивы со значениями цветов (по каналам)
 * окружающих пикселей; массивы сортируются и выбирается оказавшееся в середине значение.
 */
public class MedianMatrixProcessor extends InputCheckingMatrixProcessor {

    @Override
    protected void checkedProcess(
            double[][] matrix, int[][] source, int[][] dest, int x1, int x2, int y1, int y2
    ) {
        int height = matrix.length;
        int width = matrix[0].length;
        int radiusY = height / 2;
        int radiusX = width / 2;
        int length = matrix.length * matrix[0].length;
        int[] valuesR = new int[length];
        int[] valuesG = new int[length];
        int[] valuesB = new int[length];

        for (int i = y1; i <= y2; i++) {
            for (int j = x1; j <= x2; j++) {
                for (int y = -radiusY, ii = 0; y <= radiusY; y++, ii++) {
                    for (int x = -radiusX, jj = 0; x <= radiusX; x++, jj++) {
                        int rgb = source[y + i + radiusY][x + j + radiusX];
                        valuesR[ii * height + jj] = (rgb >> 16) & 0xff;
                        valuesG[ii * height + jj] = (rgb >> 8) & 0xff;
                        valuesB[ii * height + jj] = rgb & 0xff;
                    }
                }

                Arrays.sort(valuesR);
                Arrays.sort(valuesG);
                Arrays.sort(valuesB);
                int index = length / 2 + 1;

                dest[i][j] = valuesR[index] << 16 | valuesG[index] << 8 | valuesB[index];
            }
        }
    }

}