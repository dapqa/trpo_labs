package trpo.impl.processor;

/**
 * @author Nechaev Alexander
 * Изображение обрабатывается с помощью матрицы свертки, однако вместо значений цветов по каналам
 * используется величина яркости. В выходное изображение записывается результат обработки без какой-либо нормализации.
 */
public class BrightnessConvolutionMatrixProcessor extends InputCheckingMatrixProcessor {

    @Override
    protected void checkedProcess(
            double[][] matrix, int[][] source, int[][] dest, int x1, int x2, int y1, int y2
    ) {
        int radiusY = matrix.length / 2;
        int radiusX = matrix[0].length / 2;

        for (int i = y1; i <= y2; i++) {
            for (int j = x1; j <= x2; j++) {
                double res = 0.0;
                for (int y = -radiusY, ii = 0; y <= radiusY; y++, ii++) {
                    for (int x = -radiusX, jj = 0; x <= radiusX; x++, jj++) {
                        int rgb = source[y + i + radiusY][x + j + radiusX];
                        int r = (rgb >> 16) & 0xff;
                        int g = (rgb >> 8) & 0xff;
                        int b = rgb & 0xff;

                        // Магическая формула для определения яркости
                        double brightness = 0.2126 * r + 0.7152 * g + 0.0722 * b;

                        res += brightness * matrix[ii][jj];
                    }
                }

                int intRes = ((int) res);
                dest[i][j] = intRes;
            }
        }
    }
}
