package trpo.impl.processor;

/**
 * @author Nechaev Alexander
 * Для обработки изображений матрицами свертки. Применяется нормализация, по умолчанию
 * коэффициент нормализации равняется 1.0.
 */
public class ConvolutionMatrixProcessor extends InputCheckingMatrixProcessor {

    private double normalization = 1.0;

    @Override
    protected void checkedProcess(
            double[][] matrix, int[][] source, int[][] dest, int x1, int x2, int y1, int y2
    ) {
        int radiusY = matrix.length / 2;
        int radiusX = matrix[0].length / 2;

        for (int i = y1; i <= y2; i++) {
            for (int j = x1; j <= x2; j++) {
                double resR = 0.0;
                double resG = 0.0;
                double resB = 0.0;
                for (int y = -radiusY, ii = 0; y <= radiusY; y++, ii++) {
                    for (int x = -radiusX, jj = 0; x <= radiusX; x++, jj++) {
                        int rgb = source[y + i + radiusY][x + j + radiusX];
                        int r = (rgb >> 16) & 0xff;
                        int g = (rgb >> 8) & 0xff;
                        int b = rgb & 0xff;
                        resR += r * matrix[ii][jj];
                        resG += g * matrix[ii][jj];
                        resB += b * matrix[ii][jj];
                    }
                }

                resR /= normalization;
                resG /= normalization;
                resB /= normalization;

                int intResR = (int) resR;
                int intResG = (int) resG;
                int intResB = (int) resB;
                dest[i][j] = intResR << 16 | intResG << 8 | intResB;
            }
        }
    }

    public double getNormalization() {
        return normalization;
    }

    public void setNormalization(double normalization) {
        this.normalization = normalization;
    }
}
