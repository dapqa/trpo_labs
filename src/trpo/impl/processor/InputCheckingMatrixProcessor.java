package trpo.impl.processor;

import trpo.core.MatrixProcessor;
import trpo.core.coord.Area;
import trpo.util.CommonUtils;

/**
 * @author Nechaev Alexander
 * Перед обработкой входные значения проверяются по следующим условиям:
 * - корректность матрицы
 * - корректность размеров входного и выходного изображений
 * - область обработки входит в пределы изображения
 * Кроме того, координаты области обработки располагаются в строгом порядке: точка "от" левее и выше точки "до".
 */
abstract class InputCheckingMatrixProcessor implements MatrixProcessor {

    @Override
    public final void process(double[][] matrix, Area area, int[][] source, int[][] dest) {
        int destHeight = dest.length;
        if (destHeight == 0) return;

        int destWidth = dest[0].length;
        if (destWidth == 0) return;

        int matrixHeight, matrixWidth;
        if ((matrixHeight = matrix.length) == 0 || (matrixWidth = matrix[0].length) == 0) {
            throw new IllegalArgumentException("Матрица свертки не может быть пустой");
        }
        if (matrixHeight % 2 == 0 || matrixWidth % 2 == 0) {
            throw new IllegalArgumentException("Матрица свертки должна иметь нечетные высоту и ширину");
        }

        int radiusX = matrixWidth / 2;
        int radiusY = matrixHeight / 2;
        int sourceHeight = source.length;
        int sourceWidth = sourceHeight == 0 ? 0 : source[0].length;

        if (sourceHeight != destHeight + radiusY * 2 || sourceWidth != destWidth + radiusX * 2) {
            throw new IllegalArgumentException("Массив source должен быть расширен");
        }

        int x1 = Math.min(area.getX1(), area.getX2());
        int x2 = Math.max(area.getX1(), area.getX2());
        int y1 = Math.min(area.getY1(), area.getY2());
        int y2 = Math.max(area.getY1(), area.getY2());

        if (!CommonUtils.liesWithin(new Area(x1, y1, x2, y2), new Area(0, 0, sourceWidth - 1, sourceHeight - 1))) {
            throw new IllegalArgumentException("Область обработки выходит за пределы изображения");
        }

        checkedProcess(matrix, source, dest, x1, x2, y1, y2);
    }

    /**
     * Обработка изображения с помощью матрицы после всех произведенных проверок. Гарантируется,
     * что матрица, массивы source и dest имеют правильный размер, а область обработки лежит внутри
     * изображения.
     * @param matrix Матрица для обработки
     * @param source Исходное изображение, расширенное с учетом радиуса фильтра
     * @param dest Выходное изборажение
     * @param x1 X для левого верхнего угла
     * @param x2 X для правого нижнего угла
     * @param y1 Y для левого верхнего угла
     * @param y2 Y для правого нижнего угла
     */
    protected abstract void checkedProcess(
            double[][] matrix, int[][] source, int[][] dest,
            int x1, int x2, int y1, int y2
    );

}
