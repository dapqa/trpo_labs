package trpo;

import trpo.core.ImageProcessor;
import trpo.core.coord.Area;
import trpo.impl.filter.GaussianFilter;
import trpo.impl.filter.MedianFilter;
import trpo.impl.filter.outline.OutliningFilter;
import trpo.impl.filter.outline.OutliningMethod;

import javax.imageio.ImageIO;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;

/**
 * @author Nechaev Alexander
 */
public class Main {

    public static void main(String[] args) throws IOException {
        File outputDir = new File("./output");
        if (!outputDir.exists()) {
            if (!outputDir.mkdir()) {
                System.out.println("Не удалось создать папку output.");
                return;
            }
        }

        // Исходное изображение
        BufferedImage input = ImageIO.read(new File("./input/Horo.jpg"));
        // Исходное зашумленное изображение
        BufferedImage inputNoised = ImageIO.read(new File("./input/HoroNoised.jpg"));

        // Медианный фильтр
        BufferedImage outputMedian = ImageProcessor.filter(inputNoised, new MedianFilter().setRadius(2));
        ImageIO.write(outputMedian, "jpg", new File("./output/HoroNoisedMedian.jpg"));

        // Размытие по Гауссу прямоугольной части изображения
        BufferedImage outputGauss = ImageProcessor.filter(
                input, new GaussianFilter().setRadius(15), new Area(200, 30, 350, 180)
        );
        ImageIO.write(outputGauss, "jpg", new File("./output/HoroWithBlur.jpg"));

        // Выделение контуров по маске Превитта
        BufferedImage outputOutlinePrewitt =
                ImageProcessor.filter(input, new OutliningFilter().setMethod(OutliningMethod.PREWITT));
        ImageIO.write(outputOutlinePrewitt, "jpg", new File("./output/HoroOutlinedPrewitt.jpg"));

        // Выделение контуров по маске Собеля
        BufferedImage outputOutlineSobel =
                ImageProcessor.filter(input, new OutliningFilter().setMethod(OutliningMethod.SOBEL));
        ImageIO.write(outputOutlineSobel, "jpg", new File("./output/HoroOutlinedSobel.jpg"));

        // Выделение контуров по маске Робертса
        BufferedImage outputOutlineRoberts =
                ImageProcessor.filter(input, new OutliningFilter().setMethod(OutliningMethod.ROBERTS));
        ImageIO.write(outputOutlineRoberts, "jpg", new File("./output/HoroOutlinedRoberts.jpg"));
    }

}